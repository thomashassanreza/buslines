Busstop challange for SBAB

The application is built with Maven and with java language level 17.

How to run the application from command line:

1. Build and test applications in project root run:
```
mvn install
```

or only build:
```
mvn install -DskipTests=true
```

2. Export api key to environment variable:
```
export sl_api_key=[INSERT_VALID_API_KEY_HERE]
```

3. Run with java:
```
java -jar target/buslines-0.0.1-SNAPSHOT.jar b730a732d14d4e76b0e5e9089143d642
```
4. Goto your favorite browser:
http://localhost:8080/top10buslines
