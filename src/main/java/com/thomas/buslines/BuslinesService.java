package com.thomas.buslines;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thomas.buslines.sl.JourneyPatternPointOnLine;
import com.thomas.buslines.sl.JsonResponse;
import com.thomas.buslines.sl.StopPoint;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Scope("singleton")
public class BuslinesService {

    private final String API_KEY;

    // Cache top10 list
    private List<BuslineStopsDTO> top10;

    public BuslinesService() {
        API_KEY = System.getenv("sl_api_key");
        top10 = prepareService();
    }

    /**
     * Method to prepare the application make api calls,
     * and populate a list of the top 10 Buslines with the most stops.
     * @return top10 a sorted list of buslines with most stops
     */
    private List<BuslineStopsDTO> prepareService() {
        List<JourneyPatternPointOnLine> journeys =  getJourneyFromAPI();
        Map<Integer, String> stopPointNames = getBusStopsFromAPI();
        if (journeys == null || stopPointNames == null) {
            return null;
        }

        Map<Integer, List<String>> buslineMappedToStops = new HashMap<>();
        journeys.stream()
                .filter(journey -> journey.getDirectionCode() == 1) // Filter out the return trip
                .forEach(journey -> buslineMappedToStops.computeIfAbsent(
                        journey.getLineNumber(), k -> new ArrayList<>())
                        .add(stopPointNames.get(journey.getJourneyPatternPointNumber()))
                );
        return getTop10BusLines(buslineMappedToStops);
    }

    /**
     * Checks if top10 list is populated
     * if not calls for preparation of the service
     * otherwise:
     * @return the list
     */
    public List<BuslineStopsDTO> getTop10() {
        if (top10 == null) {
            top10 = prepareService();
        }
        return top10;
    }

    /**
     * Get the top 10 buslines sorted by the number of stops in descending order
     * @return a list of BuslineStopsDTOs
     */
    protected List<BuslineStopsDTO> getTop10BusLines(Map<Integer, List<String>> buslineMappedToStops) {
        List<BuslineStopsDTO> buslineStopsDTOs = new ArrayList<>();
        for (Integer busline : buslineMappedToStops.keySet()) {
            List<String> stops = buslineMappedToStops.get(busline);
            BuslineStopsDTO buslineStopsDTO = new BuslineStopsDTO(Integer.toString(busline), stops.size(), stops);
            buslineStopsDTOs.add(buslineStopsDTO);
        }
        Comparator<BuslineStopsDTO> reverseComparator = (buslineStopsDTO, otherBuslineStopsDTO) -> otherBuslineStopsDTO.size.compareTo(buslineStopsDTO.size);
        return buslineStopsDTOs.stream()
                        .sorted(reverseComparator)
                        .collect(Collectors.toList())
                        .subList(0,10); // Only get top 10

    }

    /**
     * Get a list of JourneyPatternPointOnLine from SL API
     * or return null if something failed.
     */
    private List<JourneyPatternPointOnLine> getJourneyFromAPI() {
        String htmlResponseBody = callSlAPIWithModel("jour");
        if (htmlResponseBody != null) {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                JsonResponse<JourneyPatternPointOnLine> jsonResponse = objectMapper.readValue(htmlResponseBody, new TypeReference<>(){});
                return jsonResponse.getResponseData().getResult();
            } catch (JsonProcessingException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Call the SL API to get bus stops
     * @return a Map of bus stop id mapped to bus stop name, or null if request failed.
     */
    private Map<Integer, String> getBusStopsFromAPI() {
        String htmlResponseBody = callSlAPIWithModel("stop");
        if (htmlResponseBody != null) {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                JsonResponse<StopPoint> jsonResponse= objectMapper.readValue(htmlResponseBody, new TypeReference<>(){});
                return jsonResponse.getResponseData().getResult().stream()
                        .collect(Collectors.toMap(StopPoint::getStopPointNumber, StopPoint::getStopPointName));

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Convenience method to call SLs API with the parameter modal
     * @param model the model we want to call the API with ex. jour
     * @return the http response body if everything went as expected null otherwise.
     */
    private String callSlAPIWithModel(String model) {
        String URL = "https://api.sl.se/api2/linedata.json?key=" + API_KEY + "&model=" + model + "&DefaultTransportModeCode=BUS";

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .GET()
                .header("accept", "application/json")
                .uri(URI.create(URL))
                .build();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            return httpResponse.body();
        } catch (IOException | InterruptedException exception) {
            exception.printStackTrace();
        }
        return null;
    }
}
