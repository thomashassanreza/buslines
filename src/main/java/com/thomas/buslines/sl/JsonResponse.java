package com.thomas.buslines.sl;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResponse<T> {

    @JsonProperty("ResponseData")
    ResponseData<T> responseData;

    public ResponseData<T> getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseData<T> responseData) {
        this.responseData = responseData;
    }
}
