package com.thomas.buslines;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Top10Controller {

    BuslinesService buslinesService;

    public Top10Controller(BuslinesService buslinesService) {
        this.buslinesService = buslinesService;
    }

    @GetMapping("/top10buslines")
    public ModelAndView top10() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("top10", buslinesService.getTop10());
        modelAndView.setViewName("top10buslines");
        return modelAndView;
    }

}

