package com.thomas.buslines;

import java.util.List;

public class BuslineStopsDTO {

    String busline;
    Integer size;
    List<String> stops;

    public BuslineStopsDTO(String busline, int size, List<String> stops) {
        this.busline = busline;
        this.size = size;
        this.stops = stops;
    }

    public String getBusline() {
        return busline;
    }

    public void setBusline(String busline) {
        this.busline = busline;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<String> getStops() {
        return stops;
    }

    public void setStops(List<String> stops) {
        this.stops = stops;
    }


    @Override
    public String toString() {
        return "BuslineStops{" +
                "busline='" + busline + '\'' +
                ", size=" + size +
                ", stops=" + stops +
                '}';
    }
}
