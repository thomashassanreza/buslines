package com.thomas.buslines;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thomas.buslines.sl.JourneyPatternPointOnLine;
import com.thomas.buslines.sl.JsonResponse;
import com.thomas.buslines.sl.StopPoint;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class BuslinesServiceTests {

    /**
     * Test BuslinesService method getTop10BusLines with mocked data
     */
    @Test
    public void testGetTop10Buslines() {
        int numberOfBusLines = 12;
        Map<Integer, List<String>> testData = getMockedBuslineToBusStops(numberOfBusLines);
        BuslinesService buslinesService = new BuslinesService();
        List<BuslineStopsDTO> buslineStopsDTOs = buslinesService.getTop10BusLines(testData);
        // Assert that there is only 10 busline DTOs
        Assert.assertEquals(buslineStopsDTOs.size(), 10);
        // Assert that the busline with most stops is found in index 0 and has the same size as
        Assert.assertEquals(buslineStopsDTOs.get(0).getSize(), numberOfBusLines);
    }

    /**
     * Test mapping of previously saved response from API into Mapped class JourneyPatternPointOnLine
     */
    @Test
    public void testParseAPIModelJourneys() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonResponse<JourneyPatternPointOnLine> jsonResponse = objectMapper.readValue(
                    new File("src/test/java/com/thomas/buslines/testJourneys"),
                    new TypeReference<>(){});

            List<JourneyPatternPointOnLine> journeys = jsonResponse.getResponseData().getResult();
            Assert.assertNotNull(journeys);
        } catch (IOException e) {
            Assert.fail("Failed to parse test API json response for Journeys. Exception: " +  e.getMessage());
        }
    }

    /**
     * Test mapping of previously saved response from API into Mapped class StopPoint
     */
    @Test
    public void testParseAPIModelStops() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonResponse<StopPoint> jsonResponse = objectMapper.readValue(
                    new File("src/test/java/com/thomas/buslines/testStops"),
                    new TypeReference<>(){});

            List<StopPoint> journeys = jsonResponse.getResponseData().getResult();
            Assert.assertNotNull(journeys);
        } catch (IOException e) {
            Assert.fail("Failed to parse test API json response for Journeys. Exception: " +  e.getMessage());
        }
    }

    /**
     * Helper method to generate testdata
     * Generates a map of integer and a list of string with the same size
     */
    private Map<Integer, List<String>> getMockedBuslineToBusStops(int sizeOfMap) {
        Map<Integer, List<String>> testData = new HashMap<>();
        for (int i = 1; i <= sizeOfMap; i++) {
            List<String> stops = new ArrayList<>();
            for (int j = 0; j < i; j++) {
                stops.add("Random bus stop name");
            }
            testData.put(i, stops);
        }
        return testData;
    }
}
